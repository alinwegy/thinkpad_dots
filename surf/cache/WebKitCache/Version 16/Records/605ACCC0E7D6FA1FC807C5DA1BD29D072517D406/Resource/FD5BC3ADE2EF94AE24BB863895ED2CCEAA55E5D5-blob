<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nerd Fonts - Iconic font aggregator, glyphs/icons collection, &amp; fonts patcher</title>
    <meta name="keywords" content="fonts collection patched-fonts powerline shell statusline python iconic-fonts patcher FontForge font-awesome octicons font-linux SourceCodePro Hack Droid Sans Meslo AnonymousPro ProFont Inconsolata">
    <meta name="description" content="Iconic font aggregator, collection, & patcher: 3,600+ glyph/icons, 40+ patched fonts: Hack, Source Code Pro, more. Popular glyph collections: Font Awesome, Octicons, Material Design Icons, and more">
    <link rel="stylesheet" href="assets/css/combo.css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700&display=swap' rel='stylesheet' type='text/css'>
    <link rel="canonical" href="https://www.nerdfonts.com" />
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
    <meta name="google-site-verification" content="492okaDWvAC6AttbBxVRWxT7-KNmPqFlLiMupaXdFU0" />
    <meta property="og:title" content="Nerd Fonts - Iconic font aggregator, glyphs/icons collection, &amp; fonts patcher">
    <meta property="og:site_name" content="Nerd Fonts">
    <meta property="og:url" content="https://www.nerdfonts.com/">
    <meta property="og:description" content="Iconic font aggregator, collection, & patcher: 3,600+ glyph/icons, 40+ patched fonts: Hack, Source Code Pro, more. Popular glyph collections: Font Awesome, Octicons, Material Design Icons, and more">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.nerdfonts.com/assets/img/sankey-glyphs-combined-diagram.png">
    
  </head>
<body>
  <div id="main">
    <nav><ul>
    
      
      
      <li class="p-home hide-on-larger-view active">
          
          <a href="#home">
          
          <i class="nf nf-fa-"></i> home</a>
      </li>
      <li class="p-home hide-on-smaller-view active">
            
            <a href="#home">
            
          <i class="nf nf-fa-"></i> home</a>
      </li>
    
      
      
      <li class="p-features hide-on-larger-view ">
          
          <a href="#features">
          
          <i class="nf nf-fa-gears"></i> Features</a>
      </li>
      <li class="p-features hide-on-smaller-view ">
            
            <a href="#features">
            
          <i class="nf nf-fa-gears"></i> Features</a>
      </li>
    
      
      
      <li class="p-downloads hide-on-larger-view ">
          
          <a href="/font-downloads">
          
          <i class="nf nf-fa-download"></i> Fonts</a>
      </li>
      <li class="p-downloads hide-on-smaller-view ">
            
            <a href="/font-downloads">
            
          <i class="nf nf-fa-download"></i> Fonts Downloads</a>
      </li>
    
      
      
      <li class="p-icon-cheat-sheet hide-on-larger-view ">
          
          <a href="/cheat-sheet">
          
          <i class="nf nf-fa-search"></i> Icons</a>
      </li>
      <li class="p-icon-cheat-sheet hide-on-smaller-view ">
            
            <a href="/cheat-sheet">
            
          <i class="nf nf-fa-search"></i> Cheat Sheet</a>
      </li>
    
      
      
      <li class="p-all-contributors hide-on-larger-view ">
          
          <a href="/contributors">
          
          <i class="nf nf-fa-flask"></i> People</a>
      </li>
      <li class="p-all-contributors hide-on-smaller-view ">
            
            <a href="/contributors">
            
          <i class="nf nf-fa-flask"></i> Contributors</a>
      </li>
    
      
      
      <li class="p-release hide-on-larger-view ">
          
          <a href="/releases">
          
          <i class="nf nf-fa-bullhorn"></i> Log</a>
      </li>
      <li class="p-release hide-on-smaller-view ">
            
            <a href="/releases">
            
          <i class="nf nf-fa-bullhorn"></i> Changelog</a>
      </li>
    
</ul></nav>
    
      
        
<div id="home" class="section p-home">
  
  <div class="icon-backdrop rainbow">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  </div>
  
  
  <div class="center container">
    <h1>
  <a href="https://github.com/ryanoasis/nerd-fonts">
    <img src="assets/img/nerd-fonts-logo.svg" alt="Nerd Fonts - Iconic font aggregator" />
  </a>
</h1>

<h3>Iconic font aggregator, collection, and patcher</h3>
<div>
<a class="github-button" href="https://github.com/ryanoasis/nerd-fonts" data-icon="octicon-star" data-size="large" data-show-count="true" data-count-aria-label="# stargazers on GitHub" aria-label="Star Nerd Fonts on GitHub">Star</a>
<a class="github-button" href="https://github.com/ryanoasis/nerd-fonts/fork" data-icon="octicon-repo-forked" data-size="large" data-count-aria-label="# forks on GitHub" data-show-count="true" aria-label="Fork ryanoasis/nerd-fonts on GitHub">Fork</a>
</div>
<div class="container">
  <div class="full column nerd-font-badges">
    <p><a href="https://github.com/ryanoasis/nerd-fonts"><img src="https://img.shields.io/github/release/ryanoasis/nerd-fonts.svg?style=for-the-badge" alt="GitHub release" /></a> <a href="https://gitter.im/ryanoasis/nerd-fonts"><img src="https://img.shields.io/gitter/room/nwjs/nw.js.svg?style=for-the-badge" alt="Gitter" /></a> <a href="#downloads" title=""><img src="https://raw.githubusercontent.com/wiki/ryanoasis/nerd-fonts/images/faux-shield-badge-os-logos.svg?sanitize=true" alt="Nerd Fonts - OS Support" /></a></p>
  </div>
</div>
<div class="container">
  <div class="full column nerd-font-buttons">
    <h3 class="inlineblock bg-blue text-white nerd-font-button">
      <i class="nf nf-fa-search"></i>
      <a href="/cheat-sheet" class="inlineblock">Icons</a>
    </h3>
    &nbsp;&nbsp;
    <h3 class="inlineblock bg-blue text-white nerd-font-button">
      <i class="nf nf-fa-download"></i>
      <a href="/font-downloads" class="inlineblock">Downloads</a>
    </h3>
  </div>
</div>
<p><strong>Nerd Fonts</strong> patches developer targeted fonts with a high number of glyphs (icons). Specifically to add a high number of extra glyphs from popular ‘iconic fonts’ such as <a href="https://github.com/FortAwesome/Font-Awesome">Font Awesome</a>, <a href="http://vorillaz.github.io/devicons/">Devicons</a>, <a href="https://github.com/github/octicons">Octicons</a>, and others.</p>
<div class="container">
<a href="https://github.com/ryanoasis/nerd-fonts">
  <img src="assets/img/sankey-glyphs-combined-diagram.png" alt="Nerd Fonts Sankey Diagram" />
</a>
<div class="text-left"><small><sub><em>Diagram created using <a href="http://sankeymatic.com/" title="SankeyMATIC (BETA): A Sankey diagram builder for everyone">SankeyMATIC</a></em></sub></small></div>
</div>
<p><span id="forkongithub">
  <a href="https://github.com/ryanoasis/nerd-fonts/tree/gh-pages">
    <i class="nf nf-fa-code_fork"></i> Fork us on GitHub <i class="nf nf-fa-heart ow"></i>
  </a>
</span></p>

<!--
Repo References
-->

<!--
Website References
-->

<!--
Link References
-->


  </div>
</div>
      
    
      
        
<div id="features" class="section p-features">
  
  
  <div class="subtlecircle sectiondivider faicon">
    <span class="fa-stack">
      <i class="nf nf-fa-circle fa-stack-2x"></i>
      <i class="nf nf-fa-gears fa-stack-1x"></i>
    </span>
    <h5 class="icon-title">Features</h5>
  </div>
  
  <div class="container">
    <h1 class="center">Features</h1>

<div class="container feature-sections">
  <div class="row">
    <div class="col-xs-12 col-md-6 col-lg-6 d-flexx">
    <div class="feature-section">
      <h2>All the icons!</h2>
      <h3>3600+ icons combined from popular sets</h3>
      <div class="row">
      <!-- <div class="col-sm-12 col-md-8 col-lg-8"> -->
      <div class="subtlecircle sectiondivider faicon sectioninner sectioninner3">
        <div>
          <b class="nf2"><i class="nf nf-seti-css"></i>.css</b> <b class="nf1"><i class="nf nf-dev-javascript"></i>.js</b><br />
          <b class="nf3"><i class="nf nf-dev-html5"></i>.html</b> <b class="nf4"><i class="nf nf-seti-json"></i>.json</b><br />
          <b class="nf6"><i class="nf nf-seti-grunt"></i> Gruntfile.json</b> <i class="nf nf-dev-react"></i>react.jsx<br />
          <b class="nf7"><i class="nf nf-dev-angular"></i>angular.js</b> <b class="nf2"><i class="nf nf-dev-jquery"></i>jquery.js</b> <b class="nf6"><i class="nf nf-seti-markdown"></i>.md</b><br />
          <b class="nf2"><i class="nf nf-dev-git"></i> git </b> <b class="nf4"><i class="nf nf-seti-php"></i> php </b> <b class="nf3"><i class="nf nf-dev-terminal"></i> .sh</b><br />
          <b class="nf2"><i class="nf nf-fa-image"></i> png </b> <b class="nf4"><i class="nf nf-dev-haskell"></i> lhs </b><br />
          <b class="nf3"><i class="nf nf-custom-folder_npm"></i> node_modules</b><br />
          <b class="nf2"><i class="nf nf-custom-folder_github"></i> .git</b><br />
        </div>
      </div>
      </div><!-- end inner row -->
      <div class="row">
      <!--</div> end inner col -->
      <!-- <div class="col-sm-12 col-md-4 col-lg-4"> -->
      <ul>
        <li><a target="_blank" aria-label="Go to XYZ Page" rel="noreferrer" href="https://github.com/ryanoasis/powerline-extra-symbols">Powerline Extra Symbols</a></li>
        <li><a target="_blank" aria-label="Go to Font Awesome Page" rel="noreferrer" href="https://github.com/FortAwesome/Font-Awesome">Font Awesome</a></li>
        <li><a target="_blank" aria-label="Go to Font Awesome Extension Page" rel="noreferrer" href="https://github.com/AndreLGava/font-awesome-extension">Font Awesome Extension</a></li>
        <li><a target="_blank" aria-label="Go to Material Design Icons Page" rel="noreferrer" href="https://github.com/Templarian/MaterialDesign">Material Design Icons</a></li>
        <li><a target="_blank" aria-label="Go to Weather Icons Page" rel="noreferrer" href="https://github.com/erikflowers/weather-icons">Weather Icons</a></li>
        <li><a target="_blank" aria-label="Go to Devicons Page" rel="noreferrer" href="http://vorillaz.github.io/devicons/">Devicons</a></li>
        <li><a target="_blank" aria-label="Go to Octicons Page" rel="noreferrer" href="https://github.com/github/octicons">Octicons</a></li>
        <li><a target="_blank" aria-label="Go to Font Logos Page" rel="noreferrer" href="https://github.com/Lukas-W/font-linux">Font Logos (Formerly Font Linux)</a></li>
        <li><a target="_blank" aria-label="Go to Pomicons Page" rel="noreferrer" href="https://github.com/gabrielelana/pomicons">Pomicons</a></li>
        <li><a target="_blank" aria-label="Go to IEC Power Symbols Page" rel="noreferrer" href="https://unicodepowersymbol.com/">IEC Power Symbols</a></li>
        <li><a target="_blank" aria-label="Go to Seti-UI + Custom Source Page" rel="noreferrer" href="https://github.com/ryanoasis/nerd-fonts/blob/master/src/glyphs/original-source.otf">Seti-UI + Custom</a></li>
      </ul>
      <!--</div> end inner col -->
      </div><!-- end inner row -->
      <h4 class="inlineblock bg-blue text-white nerd-font-button">
        <i class="nf nf-fa-search"></i>
        <a href="/cheat-sheet" class="inlineblock">Search Icons Cheat Sheet</a>
      </h4>
    </div><!-- end feature-section -->
    </div><!-- end col -->
    <div class="col-xs-12 col-md-6 col-lg-6">
    <div class="feature-section">
      <h2>The best developer fonts</h2>
      <h3>50+ patched and ready to use programming fonts</h3>
      <div class="subtlecircle sectiondivider faicon sectioninner sectioninner3">
        <div>
          <img src="/assets/img/nerd-fonts-patched-fonts.svg" alt="Preview of Patched Fonts" />
        </div>
      </div>
      <ul>
        <li>Hack</li>
        <li>FiraCode</li>
        <li>Meslo</li>
        <li>Source Code Pro</li>
        <li>Terminus</li>
        <li>Monoid</li>
        <li>Noto</li>
        <li>Iosevka</li>
        <li>and many more ...</li>
      </ul>
      <h4 class="inlineblock bg-blue text-white nerd-font-button">
        <i class="nf nf-fa-download"></i>
        <a href="/font-downloads" class="inlineblock">Downloads</a>
      </h4>
    </div><!-- end feature-section -->
    <div class="feature-section">
      <h2>Cross Platform</h2>
      <h3>Ready to use on any OS platforms and web</h3>
      <img src="https://raw.githubusercontent.com/wiki/ryanoasis/nerd-fonts/images/faux-shield-badge-os-logos.svg?sanitize=true" style="width:150px" alt="Nerd Fonts - OS Support" />
    </div><!-- end feature-section -->
    </div><!-- end col -->
  </div><!-- end row 1 -->

  <div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="feature-section">
      <h2>Supported in major projects</h2>
      <h3>Supported out of the box by many projects</h3>
      <section class="row">
        <div class="col-xs-12 col-md-3 col-lg-4">
          <a href="https://github.com/Powerlevel9k/powerlevel9k/" target="_blank" aria-label="Go to Powerlevel9k Home Page" rel="noreferrer"><h3><img src="/assets/img/pl9k-Fully-Rendered.svg" alt="Powerlevel9k" /></h3>
          <h4>Powerlevel9k</h4></a><p>The most awesome Powerline theme for ZSH around!</p>
          <h3 class="faux-logo"><a href="https://github.com/athityakumar/colorls" target="_blank" aria-label="Go to colorls Github Page" rel="noreferrer">colorls</a></h3>
          <!-- <h4>colorls</h4> --><p>A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons</p>
        </div>
        <div class="col-xs-12 col-md-3 col-lg-4">
          <a href="https://github.com/oh-my-fish/oh-my-fish" target="_blank" aria-label="Go to The Fish Shell Framework Github Page" rel="noreferrer">
          <h3><img src="/assets/img/Fish-Shell-Network.svg" alt="The Fish Shell Framework" /></h3>
          <h4>The Fish Shell Framework</h4></a><p>Oh My Fish provides core infrastructure to allow you to install packages which extend or modify the look of your shell. It's fast, extensible and easy to use.</p>
        </div>
        <div class="col-xs-12 col-md-3 col-lg-4">
          <a href="https://github.com/ryanoasis/vim-devicons" target="_blank" aria-label="Go to VimDevIcons Github Page" rel="noreferrer"><h3><img src="/assets/img/VimDevIcons.svg" alt="VimDevIcons" /></h3>
          <h4>VimDevIcons</h4></a><p>Adds Icons to your Vim Plugins</p>
          <h3 class="faux-logo"><a href="https://github.com/Peltoche/lsd" target="_blank" aria-label="Go to LSD (LSDeluxe) Github Page" rel="noreferrer">LSD (LSDeluxe)</a></h3>
          <!-- <h4>LSD (LSDeluxe)</h4> --><p>The next gen ls command. Written in Rust and fast.</p>
          ... and many more on: <a href="https://github.com/search?q=nerd+fonts&amp;type=Topics" target="_blank" aria-label="Go to Github Nerd Fonts Topic results" rel="noreferrer">Github</a> and <a href="https://gitlab.com/search?utf8=%E2%9C%93&amp;search=%22nerd+fonts%22&amp;group_id=&amp;project_id=&amp;repository_ref=" target="_blank" aria-label="Go to GitLab Nerd Fonts results" rel="noreferrer">GitLab</a></div>
      </section>
    </div>
  </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="feature-section">
      <h2>Terminal Fonts Examples</h2>
      <h3>Experiment &amp; see what's possible in the terminal with Powerline &amp; other glyphs</h3>
      <div class="row center">
        <!-- <div class="col-sm-12 col-md-7 col-lg-7"> -->
          <div class="col-lg-4">
            <div class="subtlecircle sectiondivider faicon sectioninner sectioninner3">
              <img src="/assets/img/nerd-fonts-powerline-extra-terminal.png" alt="Preview of Powerline Extra Symbols usage in terminal emulator" />
            </div>
          </div>
          <div class="col-lg-4">
            <div class="subtlecircle sectiondivider faicon sectioninner sectioninner3">
              <img src="/assets/img/nerd-fonts-icons-in-terminal.png" alt="Preview of Nerd Fonts Icons usage in terminal emulator" />
            </div>
          </div>
          <div class="col-lg-4">
            <div class="subtlecircle sectiondivider faicon sectioninner sectioninner3">
              <img src="/assets/img/nerd-fonts-icons-in-vim.png" alt="Preview of Nerd Fonts Icons usage in terminal Vim" />
            </div>
          </div>
          <p>
            All patched fonts have Powerline symbols, extra powerline symbols and many icons to choose from. Build your own status line, add icons to filetypes, make visual grepping easier. You are only limited by your imagination.
          </p>
        <!-- </div> -->
      </div><!-- end inner row -->
    </div><!-- end feature-section -->
    </div><!-- end col -->
  </div><!-- end row -->
  <div class="row">
    <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="feature-section">
      <h2>Font Patcher Script</h2>
      <h3>Create your own customized patched fonts</h3>
      <p>Use the provided <a href="https://github.com/ryanoasis/nerd-fonts#font-patcher">FontForge Python Script</a> to patch your own font or to generate over 
<strong><code class="highlighter-rouge">1.4 million</code></strong> unique combinations/variations <small><a href="https://github.com/ryanoasis/nerd-fonts#combinations">(more details)</a></small>.<br /><br />You can even specify a custom symbol font with the <strong><code class="highlighter-rouge">--custom</code></strong> option to include even more glyphs.
      </p>
<details>
<summary>Show Font Patcher CLI</summary>
<div>
            <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code><table class="rouge-table"><tbody><tr><td class="rouge-gutter gl"><pre class="lineno">1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
</pre></td><td class="rouge-code"><pre>./font-patcher
usage: font-patcher
  [-h] [-v] [-s] [-l] [-q] [-w] [-c] [--careful]
  [--removeligs] [--postprocess [POSTPROCESS]]
  [--configfile [CONFIGFILE]] [--custom [CUSTOM]]
  [-ext [EXTENSION]] [-out [OUTPUTDIR]]
  [--progressbars | --no-progressbars] [--fontawesome]
  [--fontawesomeextension] [--fontlinux] [--octicons]
  [--powersymbols] [--pomicons] [--powerline]
  [--powerlineextra] [--material] [--weather]
  font

  Nerd Fonts Font Patcher: patches a given font with programming and development related glyphs

* Website: https://www.nerdfonts.com
* Version: 2.1.0-alpha
* Development Website: https://github.com/ryanoasis/nerd-fonts
* Changelog: https://github.com/ryanoasis/nerd-fonts/blob/master/changelog.md

positional arguments:
  font                  The path to the font to patch (e.g., Inconsolata.otf)

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -s, --mono, --use-single-width-glyphs
                        Whether to generate the glyphs as single-width not double-width (default is double-width)
  -l, --adjust-line-height
                        Whether to adjust line heights (attempt to center powerline separators more evenly)
  -q, --quiet, --shutup
                        Do not generate verbose output
  -w, --windows         Limit the internal font name to 31 characters (for Windows compatibility)
  -c, --complete        Add all available Glyphs
  --careful             Do not overwrite existing glyphs if detected
  --removeligs, --removeligatures
                        Removes ligatures specificed in JSON configuration file
  --postprocess [POSTPROCESS]
                        Specify a Script for Post Processing
  --configfile [CONFIGFILE]
                        Specify a file path for JSON configuration file (see sample: src/config.sample.json)
  --custom [CUSTOM]     Specify a custom symbol font. All new glyphs will be copied, with no scaling applied.
  -ext [EXTENSION], --extension [EXTENSION]
                        Change font file type to create (e.g., ttf, otf)
  -out [OUTPUTDIR], --outputdir [OUTPUTDIR]
                        The directory to output the patched font file to
  --progressbars        Show percentage completion progress bars per Glyph Set
  --no-progressbars     Don't show percentage completion progress bars per Glyph Set

Symbol Fonts:
  --fontawesome         Add Font Awesome Glyphs (http://fontawesome.io/)
  --fontawesomeextension
                        Add Font Awesome Extension Glyphs (https://andrelzgava.github.io/font-awesome-extension/)
  --fontlinux, --fontlogos
                        Add Font Linux and other open source Glyphs (https://github.com/Lukas-W/font-logos)
  --octicons            Add Octicons Glyphs (https://octicons.github.com)
  --powersymbols        Add IEC Power Symbols (https://unicodepowersymbol.com/)
  --pomicons            Add Pomicon Glyphs (https://github.com/gabrielelana/pomicons)
  --powerline           Add Powerline Glyphs
  --powerlineextra      Add Powerline Glyphs (https://github.com/ryanoasis/powerline-extra-symbols)
  --material, --materialdesignicons, --mdi
                        Add Material Design Icons (https://github.com/templarian/MaterialDesign)
  --weather, --weathericons
                        Add Weather Icons (https://github.com/erikflowers/weather-icons)
</pre></td></tr></tbody></table></code></pre></div>            </div>
          </div>
</details>
<br />
<br />
      <h4 class="inlineblock bg-blue text-white nerd-font-button">
        <i class="nf nf-mdi-script"></i>
        <a href="https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/font-patcher" class="inlineblock">Download Patcher</a>
      </h4>
    </div><!-- end feature-section -->
    </div><!-- end col -->
  </div><!-- end row -->
 </div>

<!--
Repo References
-->


  </div>
</div>
      
    
      
    
      
    
      
    
      
    
    <div id="footer" class="section text-white">
      <div class="container">
        
        
<p>Nerd Fonts &amp; Web Site Created by <a href="https://RyanLMcIntyre.com/">Ryan L McIntyre</a>
<br />
Design by Tim O’Brien <a href="http://t413.com/">t413.com</a>
—
<a href="https://github.com/t413/SinglePaged">SinglePaged theme</a>
—
this site is <a href="https://github.com/ryanoasis/nerd-fonts/tree/gh-pages">open source</a>
<br />
Created in <i class="nf nf-dev-linux"></i> &amp; <i class="nf nf-custom-vim nfunc"></i> with <i class="nf nf-fa-heart ow"></i></p>


      </div>
    </div>
  </div>
  
  
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-91070609-1"></script>
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-91070609-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-91070609-1');
  </script>
  

  <script async defer src="https://buttons.github.io/buttons.js"></script>
<script>
((window.gitter = {}).chat = {}).options = {
  room: 'ryanoasis/nerd-fonts'
};
</script>
<script src="https://sidecar.gitter.im/dist/sidecar.v1.js" async defer></script>
<script src="site.js"></script>
<script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "SoftwareApplication",
    "name" : "Nerd Fonts - Iconic font aggregator",
    "image" : "https://www.nerdfonts.com/assets/img/sankey-glyphs-combined-diagram.png",
    "author" : {
      "@type" : "Person",
      "name" : "Ryan L McIntyre"
    },
    "downloadUrl" : "https://www.nerdfonts.com/font-downloads",
    "operatingSystem" : "Linux, macOS, Windows",
    "screenshot" : "https://www.nerdfonts.com/assets/img/nerd-fonts-powerline-extra-terminal.png",
    "softwareVersion" : "2.1.0"
  }
</script>
</body>
</html>
