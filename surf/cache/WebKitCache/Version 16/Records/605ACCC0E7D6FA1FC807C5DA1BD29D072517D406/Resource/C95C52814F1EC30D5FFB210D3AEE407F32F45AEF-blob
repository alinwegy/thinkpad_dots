/**
 * Thanks to https://github.com/gmetais/sw-remove-cookies/blob/master/remove-cookies-worker.js
 * Nice work, bro. Changed a little.
 */
(function () {
  if (
    !(
      // typeof Request === 'function' &&
      typeof fetch === 'function'
    )
  ) {
    return;
  }

  /**@type {*}*/
  // const _Request = Request;
  /**@type {*}*/
  const _fetch = fetch;
  const location = self.location;
  const locationOrigin = location.origin;
  const locationSearch = encodeURI(location.search);

  const constants = (function () {
    const DEFAULT_ASSETS_PATH_REGEXP = /\/esv4\/.+|\/sw-.+\.js/;
    const DEFAULT_PARENT_PATH_REGEXP = /^(.*)[\/#?]?$/i; // All pages.
    // const ASSETS_PATH_REGEXP = /\/esv4\/.*\.(js|css|jpg|png|gif|svg|woff|woff2|ttf|ico)$/;
    const URL_MATCHER = /^(https?:\/\/[^\/]*)(?:\/|$)/;

    return {
      URL_MATCHER,
      DEFAULT_ASSETS_PATH_REGEXP,
      DEFAULT_PARENT_PATH_REGEXP,
      COOKIE_CREDENTIALS_QUERY_SYMBOL: 'credentials',
      ASSETS_PATH_REGEXP_QUERY_SYMBOL: 'assetsPathRegexp',
      PARENT_PATH_REGEXP_QUERY_SYMBOL: 'parentPathRegexp',
      DEFAULT_COOKIE_CREDENTIALS: 'omit' // Remove cookies
    };
  })();

  const utils = (function () {
    // Cached result.
    let _cachedResults = {
      parentUrl: ''
      // presetCookieCredentials: undefined,
      // presetAssetsPathRegExp: undefined,
      // presetParentPathRegExp: undefined
    };

    const _utils = {
      hostParser (url) {
        const match = url.match(constants.URL_MATCHER);

        return match && match[1];
      },
      /**
       * Get query string
       * @param targetSearchString {string}
       * @param name {string}
       * @param [ignoreCase=false] {boolean}
       * @return {string|null}
       */
      getQueryStringFormTargetSearch (targetSearchString, name, ignoreCase) {
        if (!(targetSearchString && typeof targetSearchString === 'string')) {
          return null;
        }

        const reg = new RegExp(
          '(?:[\?|&]?)' + name + '=([^&]*)(?:&|$)',
          ignoreCase ? 'i' : undefined
        );
        const r = targetSearchString.match(reg);
        if (r != null) return decodeURIComponent(r[1]);

        return null;
      },
      /**
       * Judge enabled.
       * @param [eventReq] {Request}
       */
      getParentUrl (eventReq) {
        if (!eventReq) {
          return _cachedResults.parentUrl;
        }

        if (eventReq.mode === 'navigate') {
          return _cachedResults.parentUrl = eventReq.url;
        }

        return _cachedResults.parentUrl;
      },
      /**
       * Judge enabled.
       * @param eventReq {Request}
       * @return {boolean}
       */
      judgeReqEnableRemoveCookie (eventReq) {
        return _utils.getPresetParentPathRegExp()
            .test(_utils.getParentUrl(eventReq)) && // Judge if current page is not supported to remove cookies.
          eventReq.method === 'GET' &&
          eventReq.mode !== 'navigate' && // Not the html page.
          _utils.hostParser(eventReq.url) === locationOrigin && // same origin.
          _utils.getPresetAssetsPathRegExp().test(eventReq.url); // Matches the configuration regex
      },
      getPresetCookieCredentials () {
        if (_cachedResults.presetCookieCredentials) {
          return _cachedResults.presetCookieCredentials;
        }

        return _cachedResults.presetCookieCredentials = _utils.getQueryStringFormTargetSearch(
          locationSearch,
          constants.COOKIE_CREDENTIALS_QUERY_SYMBOL
        ) || constants.DEFAULT_COOKIE_CREDENTIALS;
      },
      getPresetAssetsPathRegExp () {
        if (_cachedResults.presetAssetsPathRegExp) {
          return _cachedResults.presetAssetsPathRegExp;
        }

        const urlPresetValue = _utils.getQueryStringFormTargetSearch(
          locationSearch,
          constants.ASSETS_PATH_REGEXP_QUERY_SYMBOL
        );

        if (!urlPresetValue) {
          return _cachedResults.presetAssetsPathRegExp = constants.DEFAULT_ASSETS_PATH_REGEXP;
        }

        const urlPresetAssetsPathRegExp = decodeURIComponent(urlPresetValue);

        if (urlPresetAssetsPathRegExp) {
          try {
            return _cachedResults.presetAssetsPathRegExp = new RegExp(
              urlPresetAssetsPathRegExp
            );
          } catch (e) {}
        }

        return _cachedResults.presetAssetsPathRegExp = constants.DEFAULT_ASSETS_PATH_REGEXP;
      },
      getPresetParentPathRegExp () {
        if (_cachedResults.presetParentPathRegExp) {
          return _cachedResults.presetParentPathRegExp;
        }

        const urlPresetValue = _utils.getQueryStringFormTargetSearch(
          locationSearch,
          constants.PARENT_PATH_REGEXP_QUERY_SYMBOL
        );

        if (!urlPresetValue) {
          return _cachedResults.presetParentPathRegExp = constants.DEFAULT_PARENT_PATH_REGEXP;
        }

        const urlPresetParentPathRegExp = decodeURIComponent(urlPresetValue);

        if (urlPresetParentPathRegExp) {
          try {
            return _cachedResults.presetParentPathRegExp = new RegExp(
              urlPresetParentPathRegExp
            );
          } catch (e) {}
        }

        return _cachedResults.presetParentPathRegExp = constants.DEFAULT_PARENT_PATH_REGEXP;
      }
    };

    return _utils;
  })();

  const events = (function () {
    return {
      _init () {
        self.addEventListener(
          'fetch',
          /** @param event {FetchEvent} */
          function (event) {
            const eventReq = event.request;

            if (utils.judgeReqEnableRemoveCookie(eventReq)) {
              // const modifiedRequest = new _Request(eventReq, {
              //   credentials: utils.getPresetCookieCredentials()
              // });

              return event.respondWith(
                _fetch(eventReq, {
                  credentials: utils.getPresetCookieCredentials()
                })
                  .then(function (response) {
                    return response;
                  })
                  .catch(function (error) {
                    console.error('[SERVICE_WORKER] Fetch failed:', error);
                  })
              );
            }
          }
        );

        /**
         * Seems that we unregister the previous sw, the current will be waiting.
         * @see https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/skipWaiting#example
         */
        self.addEventListener('install', function (event) {
          // The promise that skipWaiting() returns can be safely ignored.
          return self.skipWaiting();

          // Perform any other actions required for your
          // service worker to install, potentially inside
          // of event.waitUntil();
        });
      }
    };
  })();

  events._init();
})();
