var Progress = {
    init: function() {
        Progress._addLoadEvent(function() {
            var image = document.getElementById('progress-image');
            if (image) {
                var spinner = document.getElementById('spinner');
                if (spinner) {
                    var url = Progress._getStyle(spinner, 'background-image');
                    if (url && "none" != url) {
                        if (url.match(/^url\("/) == 'url("' || url.match(/^url\('/) == 'url(\'') {
                            image.src = url.substring("url(\"".length, url.length - 2);
                        } else {
                            image.src = url.substring("url(".length, url.length - 1);
                        }
                    }
                }
            }
        });

        document.onkeydown = Progress._keyPressed;
    },

    start: function() {
        var overlay = document.getElementById("action-overlay");
        if (overlay) {
            overlay.style.display = 'block';
            var dialog = document.getElementById("dialog-overlay");
            if (dialog) {
                dialog.style.display = 'none';
            }

            var spinner = document.getElementById("spinner");
            if (spinner) {
                window.setTimeout(function() {

                    var spinner = document.getElementById('spinner');
                    spinner.style.backgroundImage = 'none';
                    spinner.style.display = 'block';

                }, Progress._getDelay());
            }
        }
    },

    stop: function () {
        var overlay = document.getElementById("action-overlay");
        if (overlay) {
            overlay.style.display = 'none';
        }
    },

    _getStyle: function (element, cssProperty){
        var value = "";
        if(document.defaultView && document.defaultView.getComputedStyle){
            value = document.defaultView.getComputedStyle(element, "").getPropertyValue(cssProperty);
        }
        else if(element.currentStyle){
            cssProperty = cssProperty.replace(/\-(\w)/g, function (strMatch, p1){
                return p1.toUpperCase();
            });
            value = element.currentStyle[cssProperty];
        }
        return value;
    },

    _getDelay: function () {
        var image = document.getElementById('progress-image');
        if (image) {
            var src = image.src;

            if (src) {
                var delay = src.match(/.*_(\d+)\.gif$/i);
                if (delay) {
                    return parseInt(delay[1]);
                }
            }

        }
        return 1300;
    },

    _addLoadEvent: function (func) {
        var oldonload = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        } else {
            window.onload = function() {
                if (oldonload) {
                    oldonload();
                }
                func();
            }
        }
    },

    _keyPressed: function (e) {
        var event = e || window.event;
        var keyCode = event.keyCode ? event.keyCode : event.which;
        var target = event.target || event.srcElement;
        if (!(keyCode == 13 || keyCode == 3)) {
            return true;
        }
        if (event.altKey || event.metaKey) {
            return true;
        }

        if (Progress._handleDefaultButton(event, target)) {
            return false;
        }
        return true;
    },

    _handleDefaultButton: function (event, target) {
        if (!target || (target.type != 'submit' && target.type != 'image')) {
            if (event.ctrlKey || !target || !Progress._isTextArea(target)) {
                var d = document.getElementById("default-button");
                if (d) {
                    d.click();
                    event.cancelBubble = true;
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    }
                    return true;
                }
            }
        }
        return false;
    },

    _isTextArea: function (target) {
        return target.nodeName == "textarea" || target.nodeName == "TEXTAREA";
    }
};

Progress.init();
