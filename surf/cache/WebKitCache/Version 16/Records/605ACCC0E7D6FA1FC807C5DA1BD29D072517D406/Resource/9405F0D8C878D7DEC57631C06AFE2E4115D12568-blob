'use strict';
$.fn.reverse = [].reverse;


function getSides(obj, includeBorder)
{
  var result = {};

  if (includeBorder)
  {
    result.n = obj.offset().top;
    result.w = obj.offset().left;
    result.s = result.n + obj.outerHeight();
    result.e = result.w + obj.outerWidth();
  }
  else
  {
        result.n = obj.offset().top + parseFloat(obj.css('border-top-width'));
    result.w = obj.offset().left + parseFloat(obj.css('border-left-width'));
    result.s = result.n + obj.innerHeight();
    result.e = result.w + obj.innerWidth();
  }

  result.top = result.n;
  result.left = result.w;
  result.bottom = result.s;
  result.right = result.e;

  return result;
}


function rectanglesIdentical(r1, r2)
{
    return ((Math.floor(Math.abs(r1.n - r2.n)) <= 1) &&
          (Math.floor(Math.abs(r1.s - r2.s)) <= 1) &&
          (Math.floor(Math.abs(r1.e - r2.e)) <= 1) &&
          (Math.floor(Math.abs(r1.w - r2.w)) <= 1));
}


function rectanglesOverlap(r1, r2)
{
    if ((Math.round(r1.n - r2.s) >= 0) ||
      (Math.round(r1.s - r2.n) <= 0) ||
      (Math.round(r1.w - r2.e) >= 0) ||
      (Math.round(r1.e - r2.w) <= 0))
  {
    return false;
  }
  else
  {
    return true;
  }
}


function getClosestSide(rectangles, side)
{
  var result = null;

  rectangles.forEach(function(rectangle) {
      if (result === null)
      {
        result = rectangle[side];
      }
      else if ((side === 'e') || (side === 's'))
      {
        result = Math.max(result, rectangle[side]);
      }
      else
      {
        result = Math.min(result, rectangle[side]);
      }
    });

  return result;
}


function getDataName(jqObject)
{
  var possibleNames = ['room', 'date', 'seconds'];
  for (var i=0; i<possibleNames.length; i++)
  {
    if (jqObject.data(possibleNames[i]) !== undefined)
    {
      return possibleNames[i];
    }
  }
  return false;
}


var Table = {
  selector: ".dwm_main:not('#month_main')",
  borderLeftWidth: undefined,
  borderTopWidth: undefined,
  bookedMap: [],
  grid: {},

    getBookingParams: function(el) {
      var rtl = ($(Table.selector).css('direction').toLowerCase() === 'rtl'),
          params = {},
          data,
          tolerance = 2,           cell = {x: {}, y: {}},
          i,
          axis;

      cell.x.start = el.offset().left;
      cell.y.start = el.offset().top;
      cell.x.end = cell.x.start + el.outerWidth();
      cell.y.end = cell.y.start + el.outerHeight();
      for (axis in cell)
      {
        if (cell.hasOwnProperty(axis))
        {
          data = Table.grid[axis].data;
          if (params[Table.grid[axis].key] === undefined)
          {
            params[Table.grid[axis].key] = [];
          }
          if (rtl && (axis==='x'))
          {
            for (i = data.length - 1; i >= 0; i--)
            {
              if ((data[i].coord + tolerance) < cell[axis].start)
              {
                                if ((Table.grid[axis].key === 'seconds') ||
                    (params[Table.grid[axis].key].length === 0))
                {
                  params[Table.grid[axis].key].push(data[i].value);
                }
                break;
              }
              if ((data[i].coord + tolerance) < cell[axis].end)
              {
                params[Table.grid[axis].key].push(data[i].value);
              }
            }
          }
          else
          {
            for (i=0; i<data.length; i++)
            {
              if ((data[i].coord + tolerance) > cell[axis].end)
              {
                                if ((Table.grid[axis].key === 'seconds') ||
                    (params[Table.grid[axis].key].length === 0))
                {
                  params[Table.grid[axis].key].push(data[i].value);
                }
                break;
              }
              if ((data[i].coord + tolerance) > cell[axis].start)
              {
                params[Table.grid[axis].key].push(data[i].value);
              }
            }           }
        }
      }       return params;
    },  

  getRowNumber: function(y) {
      for (var i=0; i<Table.grid.y.data.length - 1; i++)
      {
        if ((y >= Table.grid.y.data[i].coord) &&
            (y < Table.grid.y.data[i+1].coord))
        {
          return i;
        }
      }
      return null;
    },  

    clearRowLabels: function() {
      if (Table.highlightRowLabels.rows !== undefined)
      {
        for (var i=0; i < Table.highlightRowLabels.rows.length; i++)
        {
          Table.highlightRowLabels.rows[i].removeClass('selected');
        }
      }
    },  

    highlightRowLabels: function (el) {
      if (Table.highlightRowLabels.rows === undefined)
      {
                Table.highlightRowLabels.rows = [];
        $(Table.selector).find('tbody tr').each(function() {
            Table.highlightRowLabels.rows.push($(this).find('th'));
          });
      }
      var elStartRow = Table.getRowNumber(el.offset().top);
      var elEndRow = Table.getRowNumber(el.offset().top + el.outerHeight());
      for (var i=0; i<Table.highlightRowLabels.rows.length ; i++)
      {
        if (((elStartRow === null) || (elStartRow <= i)) &&
            ((elEndRow === null) || (i < elEndRow)))
        {
          Table.highlightRowLabels.rows[i].addClass('selected');
        }
        else
        {
          Table.highlightRowLabels.rows[i].removeClass('selected');
        }
      }
    },  

  init: function() {
    var table = $(Table.selector);
    var container = table.parent();
        Table.bookedMap = [];
    table.find('td.booked:visible').each(function() {
        Table.bookedMap.push(getSides($(this)));
      });
        Table.size();
  },


    outside: function(p) {
      var headBottoms = $(Table.selector).find('thead').map(function() {
          return $(this).offset().top + $(this).outerHeight();
        }).get();

            if (p.y < (Math.max.apply(null, headBottoms)))
      {
        return true;
      }

      return ((p.x < Table.grid.x.data[0].coord) ||
              (p.y < Table.grid.y.data[0].coord) ||
              (p.x > Table.grid.x.data[Table.grid.x.data.length - 1].coord) ||
              (p.y > Table.grid.y.data[Table.grid.y.data.length - 1].coord) );
    },  

    overlapsBooked: function overlapsBooked(rectangle, stopAtFirst, ignoreRectangle)
  {
    var result = [];

    for (var i=0; i<Table.bookedMap.length; i++)
    {
      if (!(ignoreRectangle && rectanglesIdentical(ignoreRectangle, Table.bookedMap[i])))
      {
        if (rectanglesOverlap(rectangle, Table.bookedMap[i]))
        {
          result.push(Table.bookedMap[i]);
          if (stopAtFirst)
          {
            break;
          }
        }
      }
    }

    return result;
  },


    setClipPath: function(el) {
      var path = 'none';
            var th = $(Table.selector).find('thead tr:last-child th:first-child');
      var theadBottom = th.offset().top + th.outerHeight();
      var elTop = el.offset().top;
      var elHeight = el.outerHeight();
      var above = theadBottom - elTop;

      if (above > 0)
      {
        path = 'inset(' + above + 'px 0 0 0)';
      }

      el.css('clip-path', path);
    },


  size: function() {
            if ((args.view == 'week') && args.view_all)
      {
        return;
      }
            var table = $(Table.selector);
      var td = table.find('tbody tr:first-child td:first-of-type');
      Table.borderLeftWidth = parseFloat(td.css('border-left-width'));
      Table.borderTopWidth = parseFloat(td.css('border-top-width'));

            var rtl = ((table.css('direction') !== undefined) &&
                 (table.css('direction').toLowerCase() === 'rtl'));
      var resolution = table.data('resolution');
      Table.grid.x = {};
      Table.grid.x.data = [];
            var columns = table.find('thead tr:first-child th:visible').not('.first_last');

            if (rtl)
      {
        columns.reverse();
      }
      columns.each(function() {
          if (Table.grid.x.key === undefined)
          {
            Table.grid.x.key = getDataName($(this));
          }
          Table.grid.x.data.push({coord: $(this).offset().left,
                                  value: $(this).data(Table.grid.x.key)});
        });
            if (rtl)
      {
        columns.filter(':first').each(function() {
            var value = null;
            if (Table.grid.x.key === 'seconds')
            {
              value = Table.grid.x.data[0].value + resolution;
            }
            var edge = $(this).offset().left;
            Table.grid.x.data.unshift({coord: edge, value: value});
          });
      }

      columns.filter(':last').each(function() {
          var value = null;
          if (Table.grid.x.key === 'seconds')
          {
            value = Table.grid.x.data[Table.grid.x.data.length - 1].value + resolution;
          }
          var edge = $(this).offset().left + $(this).outerWidth();
          Table.grid.x.data.push({coord: edge, value: value});
        });


      Table.grid.y = {};
      Table.grid.y.data = [];
      var rows = table.find('tbody th:first-child');
      rows.each(function() {
          if (Table.grid.y.key === undefined)
          {
            Table.grid.y.key = getDataName($(this));
          }
          Table.grid.y.data.push({coord: $(this).offset().top,
                                  value: $(this).data(Table.grid.y.key)});
        });
            rows.filter(':last').each(function() {
          var value = null;
          if (Table.grid.y.key === 'seconds')
          {
            value = Table.grid.y.data[Table.grid.y.data.length - 1].value + resolution;
          }
          Table.grid.y.data.push({coord: $(this).offset().top + $(this).outerHeight(),
                                  value: value});
        });
    }, 
    snapToGrid: function (obj, side, force) {
      var snapGap = 35,           tolerance = 2,           isLR = (side === 'left') || (side === 'right'),
          data = (isLR) ? Table.grid.x.data : Table.grid.y.data,
          topLeft, bottomRight, gap, gapTopLeft, gapBottomRight;

      var rectangle = obj.offset();
          rectangle.bottom = rectangle.top + obj.innerHeight();
          rectangle.right = rectangle.left + obj.innerWidth();

      var outerWidth = rectangle.right - rectangle.left,
          outerHeight = rectangle.bottom - rectangle.top,
          thisCoord = rectangle[side];

      for (var i=0; i<(data.length -1); i++)
      {
        topLeft = data[i].coord;
        bottomRight = data[i+1].coord;
                if (side === 'left')
        {
          topLeft += Table.borderLeftWidth;
          bottomRight += Table.borderLeftWidth;
        }
        else if (side === 'top')
        {
          topLeft += Table.borderTopWidth;
          bottomRight += Table.borderTopWidth;
        }

        gapTopLeft = thisCoord - topLeft;
        gapBottomRight = bottomRight - thisCoord;

        if (((gapTopLeft > 0) && (gapBottomRight > 0)) ||
                        ((i === 0) && (gapTopLeft < 0)) ||
            ((i === (data.length-2)) && (gapBottomRight < 0)) )
        {
          gap = bottomRight - topLeft;

          if ((gapTopLeft <= gap/2) && (force || (gapTopLeft < snapGap)))
          {
            switch (side)
            {
              case 'left':
                obj.offset({top: rectangle.top, left: topLeft});
                obj.outerWidth(outerWidth + gapTopLeft);
                break;

              case 'right':
                                if ((outerWidth - gapTopLeft) < tolerance)
                {
                  obj.outerWidth(outerWidth + gapBottomRight);
                }
                else
                {
                  obj.outerWidth(outerWidth - gapTopLeft);
                }
                break;

              case 'top':
                obj.offset({top: topLeft, left: rectangle.left});
                obj.outerHeight(outerHeight + gapTopLeft);
                break;

              case 'bottom':
                                if ((outerHeight - gapTopLeft) < tolerance)
                {
                  obj.outerHeight(outerHeight + gapBottomRight);
                }
                else
                {
                  obj.outerHeight(outerHeight - gapTopLeft);
                }
                break;
            }
            return;
          }
          else if ((gapBottomRight <= gap/2) && (force || (gapBottomRight < snapGap)))
          {
            switch (side)
            {
              case 'left':
                                if ((outerWidth - gapBottomRight) < tolerance)
                {
                  obj.offset({top: rectangle.top, left: topLeft});
                  obj.outerWidth(outerWidth + gapTopLeft);
                }
                else
                {
                  obj.offset({top: rectangle.top, left: bottomRight});
                  obj.outerWidth(outerWidth - gapBottomRight);
                }
                break;

              case 'right':
                obj.outerWidth(outerWidth + gapBottomRight);
                break;

              case 'top':
                                if ((outerHeight - gapBottomRight) < tolerance)
                {
                  obj.offset({top: topLeft, left: rectangle.left});
                  obj.outerHeight(outerHeight + gapTopLeft);
                }
                else
                {
                  obj.offset({top: bottomRight, left: rectangle.left});
                  obj.outerHeight(outerHeight - gapBottomRight);
                }
                break;

              case 'bottom':
                obj.outerHeight(outerHeight + gapBottomRight);
                break;
            }
            return;
          }
        }
      }      }  
};


$(document).on('page_ready', function() {

    $(Table.selector).on('tableload', function() {
      var table = $(this);

            if (((args.view == 'week') && args.view_all) ||
          table.find('tbody').data('empty'))
      {
        return;
      }

      Table.init();

      var mouseDown = false;

      var downHandler = function(e) {
          mouseDown = true;

                    table.addClass('resizing');

          var jqTarget = $(e.target);
                    if (e.target.nodeName.toLowerCase() === "img")
          {
            jqTarget = jqTarget.parent();
          }
          downHandler.origin = jqTarget.offset();
          downHandler.firstPosition = {x: e.pageX, y: e.pageY};
                    downHandler.originalLink = jqTarget.find('a').addBack('a').attr('href');
          downHandler.box = $('<div class="div_select">');

          if (!args.isBookAdmin)
          {
                        if (((args.view == 'week') && true) ||
                ((args.view == 'day') && true))
            {
                              var slotHeight = jqTarget.outerHeight();
                downHandler.maxHeight = true;
                downHandler.box.css('max-height', slotHeight + 'px');
                downHandler.box.css('min-height', slotHeight + 'px');
                            }
          }

                    $(document.body).append(downHandler.box);
          downHandler.box.offset(downHandler.origin);
        };

      var moveHandler = function(e) {
          var box = downHandler.box;
          var oldBoxOffset = box.offset();
          var oldBoxWidth = box.outerWidth();
          var oldBoxHeight = box.outerHeight();

                    if ((downHandler.maxWidth && (e.pageX < downHandler.origin.left)) ||
              (downHandler.maxHeight && (e.pageY < downHandler.origin.top)))
          {
            return;
          }
                    if (e.pageX < downHandler.origin.left)
          {
            if (e.pageY < downHandler.origin.top)
            {
              box.offset({top: e.pageY, left: e.pageX});
            }
            else
            {
              box.offset({top: downHandler.origin.top, left: e.pageX});
            }
          }
          else if (e.pageY < downHandler.origin.top)
          {
            box.offset({top: e.pageY, left: downHandler.origin.left});
          }
          else
          {
            box.offset(downHandler.origin);
          }
          box.width(Math.abs(e.pageX - downHandler.origin.left));
          box.height(Math.abs(e.pageY - downHandler.origin.top));
          Table.snapToGrid(box, 'top');
          Table.snapToGrid(box, 'bottom');
          Table.snapToGrid(box, 'right');
          Table.snapToGrid(box, 'left');
                    if (Table.overlapsBooked(getSides(box), true).length)
          {
            box.offset(oldBoxOffset)
               .width(oldBoxWidth)
               .height(oldBoxHeight);
          }
                    if (Table.outside({x: e.pageX, y: e.pageY}))
          {
            if (!moveHandler.outside)
            {
              box.addClass('outside');
              moveHandler.outside = true;
              Table.clearRowLabels();
            }
          }
          else if (moveHandler.outside)
          {
            box.removeClass('outside');
            moveHandler.outside = false;
          }
                    if (!moveHandler.outside)
          {
            Table.highlightRowLabels(box);
          }
        };


      var upHandler = function(e) {
          mouseDown = false;
          e.preventDefault();
          var tolerance = 2;           var box = downHandler.box;
          var params = Table.getBookingParams(box);
          $(document).off('mousemove',moveHandler);
          $(document).off('mouseup', upHandler);


                    if (Table.outside({x: e.pageX, y: e.pageY}))
          {
            box.remove();
            $(Table.selector).removeClass('resizing');
            return;
          }
                    else if ((Math.abs(e.pageX - downHandler.firstPosition.x) <= tolerance) &&
                   (Math.abs(e.pageY - downHandler.firstPosition.y) <= tolerance))
          {
            if (downHandler.originalLink !== undefined)
            {
              window.location = downHandler.originalLink;
            }
            else
            {
              box.remove();
              $(Table.selector).removeClass('resizing');
            }
            return;
          }
                    var queryString = 'drag=1';            queryString += '&area=' + args.area;
          queryString += '&start_seconds=' + params.seconds[0];
          queryString += '&end_seconds=' + params.seconds[params.seconds.length - 1];
          if (args.view === 'day')
          {
            for (var i=0; i<params.room.length; i++)
            {
              queryString += '&rooms[]=' + params.room[i];
            }
            queryString += '&start_date=' + args.pageDate;
          }
          else           {
            queryString += '&rooms[]=' + args.room;
            queryString += '&start_date=' + params.date[0];
            queryString += '&end_date=' + params.date[params.date.length - 1];
          }
          if (args.site)
          {
            queryString += '&site=' + encodeURIComponent(args.site);
          }
          window.location = 'edit_entry.php?' + queryString;
          return;
        };


            var resize = function(event, ui)
      {
        var closest,
            rectangle = {},
            sides = {n: false, s: false, e: false, w: false};

        if (resize.lastRectangle === undefined)
        {
          resize.lastRectangle = $.extend({}, resizeStart.originalRectangle);
        }

                if (Math.round(ui.position.top - ui.originalPosition.top) === 0)
        {
          rectangle.n = ui.position.top;
        }
        else
        {
          rectangle.n = event.pageY;
          sides.n = true;
        }

        if (Math.round(ui.position.left - ui.originalPosition.left) === 0)
        {
          rectangle.w = ui.position.left;
        }
        else
        {
          rectangle.w = event.pageX;
          sides.w = true;
        }

        if (Math.round((ui.position.top + ui.size.height) -
                       (ui.originalPosition.top + ui.originalSize.height)) === 0)
        {
          rectangle.s = ui.position.top + ui.size.height;
        }
        else
        {
          rectangle.s = event.pageY;
          sides.s = true;
        }

        if (Math.round((ui.position.left + ui.size.width) -
                       (ui.originalPosition.left + ui.originalSize.width)) === 0)
        {
          rectangle.e = ui.position.left + ui.size.width;
        }
        else
        {
          rectangle.e = event.pageX;
          sides.e = true;
        }

                var overlappedElements = Table.overlapsBooked(rectangle, false, resizeStart.originalRectangle);

        if (!overlappedElements.length)
        {
                    ui.element.resizable('option', {maxHeight: null,
                                          maxWidth: null});
        }
        else
        {
                    if (sides.n)
          {
            closest = getClosestSide(overlappedElements, 's');
            if (event.pageY <= closest)
            {
              ui.position.top = closest + Table.borderTopWidth;
              ui.element.resizable('option', 'maxHeight', ui.originalSize.height + ui.originalPosition.top - ui.position.top);
            }
            else
            {
              ui.element.resizable('option', 'maxHeight', null);
            }
          }

          if (sides.w)
          {
            closest = getClosestSide(overlappedElements, 'e');
            if (event.pageX <= closest)
            {
              ui.position.left = closest + Table.borderLeftWidth;
              ui.element.resizable('option', 'maxWidth', ui.originalSize.width + ui.originalPosition.left - ui.position.left);
            }
            else
            {
              ui.element.resizable('option', 'maxWidth', null);
            }
          }

          if (sides.s)
          {
            closest = getClosestSide(overlappedElements, 'n');
            if (event.pageY >= closest)
            {
              ui.element.resizable('option', 'maxHeight', closest - ui.originalPosition.top);
            }
            else
            {
              ui.element.resizable('option', 'maxHeight', null);
            }
          }

          if (sides.e)
          {
            closest = getClosestSide(overlappedElements, 'w');
            if (event.pageX >= closest)
            {
              ui.element.resizable('option', 'maxWidth', closest - ui.originalPosition.left);
            }
            else
            {
              ui.element.resizable('option', 'maxWidth', null);
            }
          }
        }


        
                if (sides.w)
        {
          Table.snapToGrid(ui.helper, 'left');
        }
                if (sides.e)
        {
          Table.snapToGrid(ui.helper, 'right');
        }
                if (sides.n)
        {
          Table.snapToGrid(ui.helper, 'top');
        }
                if (sides.s)
        {
          Table.snapToGrid(ui.helper, 'bottom');
        }

        resize.lastRectangle = $.extend({}, rectangle);

        Table.highlightRowLabels(ui.helper);

        Table.setClipPath(ui.helper);
      };  

            var resizeStart = function(event, ui)
      {
        resizeStart.oldParams = Table.getBookingParams(ui.originalElement.find('a'));

        resizeStart.originalRectangle = {
            n: ui.originalPosition.top,
            s: ui.originalPosition.top + ui.originalSize.height,
            w: ui.originalPosition.left,
            e: ui.originalPosition.left + ui.originalSize.width
          };

                table.addClass('resizing');
      };  

            var resizeStop = function(event, ui)
      {
                ['left', 'right', 'top', 'bottom'].forEach(function(side) {
            Table.snapToGrid(ui.helper, side, true);
            Table.snapToGrid(ui.element, side, true);
          });

        if (rectanglesIdentical(resizeStart.originalRectangle, getSides(ui.helper)))
        {
                    ui.element.css({width: '100%', height: '100%'});
          $(Table.selector).removeClass('resizing');
          return;
        }

                var data = {csrf_token: getCSRFToken(),
                    commit: 1},
            booking = ui.element.find('a');
                data.id = booking.data('id');
        data.type = booking.data('type');
                var oldParams = resizeStart.oldParams;
        var newParams = Table.getBookingParams(booking);
        if (newParams.seconds !== undefined)
        {
                    if (newParams.seconds[0] !== oldParams.seconds[0])
          {
            data.start_seconds = newParams.seconds[0];
          }
          if (newParams.seconds[newParams.seconds.length - 1] !==
              oldParams.seconds[oldParams.seconds.length - 1])
          {
            data.end_seconds = newParams.seconds[newParams.seconds.length - 1];
                      }
        }
        data.view = args.view;
        data.view_all = args.view_all;
        if (args.view === 'day')
        {
          data.start_date = args.pageDate;

        }
        else          {
          data.start_date = newParams.date[0];
          var onlyAdminCanBookRepeat = true;
          if (args.isBookAdmin || !onlyAdminCanBookRepeat)
          {
            if (newParams.date.length > 1)
            {
              data.rep_type = 1;
              data.rep_interval = 1;
              data.rep_end_date = newParams.date[newParams.date.length - 1];
            }
          }
        }
        data.end_date = data.start_date;
        data.rooms = (typeof newParams.room === 'undefined') ? args.room : newParams.room;
                booking.addClass('saving')
               .after('<span class="saving">Saving</span>');

        if(args.site)
        {
          data.site = args.site;
        }

        $.post('edit_entry_handler.php',
               data,
               function(result) {
                                    table.empty()
                       .html(result.table_innerhtml)
                       .trigger('tableload');
                                    if (!result.valid_booking)
                  {
                    var alertMessage = '';
                    if (result.conflicts.length > 0)
                    {
                      alertMessage += 'The new booking will conflict with the following entries:' + ":  \n\n";
                      var conflictsList = getErrorList(result.conflicts);
                      alertMessage += conflictsList.text;
                    }
                    if (result.violations.errors.length > 0)
                    {
                      if (result.conflicts.length > 0)
                      {
                        alertMessage += "\n\n";
                      }
                      alertMessage += 'The new booking will conflict with the following policies:' + ":  \n\n";
                      var rulesList = getErrorList(result.violations.errors);
                      alertMessage += rulesList.text;
                    }
                    window.alert(alertMessage);
                  }
                },
               'json');

      };  

            table.find('td.new').each(function() {
          $(this).find('a').on('click', function(event) {
              event.preventDefault();
            });
          $(this).on('mousedown', function(event) {
              event.preventDefault();
              downHandler(event);
              $(document).on('mousemove', moveHandler);
              $(document).on('mouseup', upHandler);
            });
        });



            table.find('.writable')
        .each(function() {

                        var directions = {times: {plus: true, minus: true},
                              other: {plus: true, minus: true}};

            if ($(this).hasClass('series'))
            {
                            directions.other = {plus: false, minus: false};
            }
            if (!args.isBookAdmin)
            {
              if (((args.view == 'week') && true) ||
                  ((args.view == 'day') && true))
              {
                                directions.other = {plus: false, minus: false};
              }
                              if ($(this).hasClass('multiday_start') ||
                    $(this).hasClass('multiday_end'))
                {
                  directions.times = {plus: false, minus: false};
                }
                            }
                        if ($(this).hasClass('multiday_start'))
            {
              directions.times.minus = false;
              directions.other = {plus: false, minus: false};
            }
            if ($(this).hasClass('multiday_end'))
            {
              directions.times.plus = false;
              directions.other = {plus: false, minus: false};
            }
                        var aHandles = [];
            if (directions.times.plus)
            {
              aHandles.push('e');
            }
            if (directions.times.minus)
            {
              aHandles.push('w');
            }
            if (directions.other.plus)
            {
              aHandles.push('s');
            }
            if (directions.other.minus)
            {
              aHandles.push('n');
            }
                        ['nw', 'ne', 'se', 'sw'].forEach(function(corner) {
                if ((aHandles.indexOf(corner[0]) >= 0) &&
                    (aHandles.indexOf(corner[1]) >= 0))
                {
                  aHandles.push(corner);
                }
              });

            var handles = aHandles.join(',');

            if (handles)
            {
              $(this).resizable({handles: handles,
                                 helper: 'resizable-helper',
                                 start: resizeStart,
                                 resize: resize,
                                 stop: resizeStop});
            }

            $(this).css('background-color', 'transparent');
          });

            $('.ui-resizable-handle')
        .on('mouseenter', function(e) {
            if (!mouseDown)
            {
              if ($(this).is(':hover'))
              {
                table.addClass('resizing');
              }
              else
              {
                table.removeClass('resizing');
              }
            }
          })
        .on('mouseleave', function() {
            if (!mouseDown)
            {
              table.removeClass('resizing');
            }
          })
        .on('mousedown', function() {
            mouseDown = true;
            if ($(this).is(':hover'))
            {
              table.addClass('resizing');
            }
          })
        .on('mouseup', function() {
            mouseDown = false;
            if (!$(this).is(':hover'))
            {
              table.removeClass('resizing');
            }
          })
        .first().trigger('mouseenter');

    }).trigger('tableload');

  $(window).on('resize', throttle(function(event) {
      if (event.target === this)        {
                Table.init();
      }
    }, 50));

    $(Table.selector).parent().on('scroll', throttle(function() {
      Table.init();
    }));

});

