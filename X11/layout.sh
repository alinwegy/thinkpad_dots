#!/bin/bash
monitors=$(xrandr | grep " connected" | cut -d" " -f1) 
[[ $(echo $monitors | grep -w "eDP1") ]] && xrandr --output eDP1 --primary --mode 1366x768 --pos 0x312 --rotate normal --gamma 0.7:0.7:0.6 && echo "eDP1 connected"
[[ $(echo $monitors | grep -w "DP1") ]] && xrandr --output DP1 --off && echo "DP1 connected"
[[ $(echo $monitors | grep -w "HDMI1") ]] && xrandr --output HDMI1 --mode 1920x1080 --pos 1366x0 --rotate normal && echo "HDMI1 connected"
[[ $(echo $monitors | grep -w "HDMI2") ]] && xrandr --output HDMI2 --off && echo "HDMI2 connected"
[[ $(echo $monitors | grep -w "VIRTUAL1") ]] && xrandr --output VIRTUAL1 --off && echo "VIRTUAL1 connected"
